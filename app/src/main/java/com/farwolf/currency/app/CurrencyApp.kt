package com.farwolf.currency.app

import android.app.Application
import com.farwolf.currency.data.model.Currency
import com.farwolf.currency.data.network.networkModule
import com.farwolf.currency.ui.currency.currencyModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CurrencyApp : Application() {

    companion object {
        private lateinit var app: CurrencyApp
        fun app(): CurrencyApp {
            return app
        }
        var baseCountry = Currency.EUR
    }

    override fun onCreate() {
        super.onCreate()
        app = this
        startKoin {
            androidContext(this@CurrencyApp)
            modules(
                listOf(
                    networkModule,
                    currencyModule
                )
            )
        }
    }
}