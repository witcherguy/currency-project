package com.farwolf.currency.domain

import com.farwolf.currency.data.model.Currency
import com.farwolf.currency.data.model.Exchanger

class GetCurrencyUseCase(
    private val currencyRepository: ICurrencyRepository
) {

    suspend fun updateCurrency(currency: Currency): Exchanger {
        return currencyRepository.getCurrency(currency)
    }
}