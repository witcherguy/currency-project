package com.farwolf.currency.domain

import com.farwolf.currency.data.model.Currency
import com.farwolf.currency.data.model.Exchanger

interface ICurrencyRepository {

    suspend fun getCurrency(currency: Currency): Exchanger
}