package com.farwolf.currency.util

import com.farwolf.currency.ui.currency.item.MoneyExchangerItem

object Converter {

    fun convertCurrency(baseValue: Double, moneyExchangerItem: MoneyExchangerItem): Double {
        return baseValue * moneyExchangerItem.value
    }

    fun getBaseValue(currentValue: Double, moneyExchangerItem: MoneyExchangerItem) : Double {
        return currentValue / moneyExchangerItem.value
    }
}