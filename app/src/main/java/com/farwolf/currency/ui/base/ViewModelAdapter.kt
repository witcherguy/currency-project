package com.farwolf.currency.ui.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.UiThread
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class ViewModelAdapter : RecyclerView.Adapter<ViewModelAdapter.ViewHolder>() {

    data class CellInfo(val layoutId: Int, val bindingId: Int)

    private val cellMap = HashMap<Class<out Any>, CellInfo>()
    private val sharedObjects = hashMapOf<Int, Any>()

    var items = arrayOf<Any>()
        @UiThread
        set(value) {
            try {
                val callback = DiffCallback(field, value)
                val diffResult = DiffUtil.calculateDiff(callback)
                field = value
                diffResult.dispatchUpdatesTo(this)
            } catch (ex: NotImplementedError) {
                field = value
                notifyDataSetChanged()
            }
        }

    protected fun cell(clazz: Class<out Any>, @LayoutRes layoutId: Int, bindingId: Int) {
        cellMap[clazz] = CellInfo(layoutId, bindingId)
    }

    private fun getCellInfo(viewModel: Any): CellInfo {
        cellMap.entries
            .filter { it.key == viewModel.javaClass }
            .first { return it.value }

        throw Exception("Cell info for class ${viewModel.javaClass.name} not found.")
    }

    protected fun sharedObject(sharedObject: Any, bindingId: Int) {
        sharedObjects[bindingId] = sharedObject
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        return getCellInfo(items[position]).layoutId
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(viewType, parent, false)
        val viewHolder = ViewHolder(view)

        sharedObjects.forEach { viewHolder.binding?.setVariable(it.key, it.value) }

        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cellInfo = getCellInfo(items[position])
        if (cellInfo.bindingId != 0) {
            holder.binding?.setVariable(cellInfo.bindingId, items[position])
        }
    }

    open fun replaceItems(items: List<Any>) {
        this.items = items.toTypedArray()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view)  {

        val binding = DataBindingUtil.bind<ViewDataBinding>(view)
    }

    protected open fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
        throw NotImplementedError()
    }

    protected open fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
        return areItemsTheSame(oldItem, newItem)
    }

    protected open fun getChangePayload(oldItem: Any, newItem: Any): Any? {
        return null
    }

    private inner class DiffCallback(
            val oldItems: Array<Any>,
            val newItems: Array<Any>)
        : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]
            return this@ViewModelAdapter.areItemsTheSame(oldItem, newItem)
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]
            return this@ViewModelAdapter.areContentsTheSame(oldItem, newItem)
        }

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]
            return this@ViewModelAdapter.getChangePayload(oldItem, newItem)
        }

    }
}