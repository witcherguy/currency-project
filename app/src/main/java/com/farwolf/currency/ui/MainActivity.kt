package com.farwolf.currency.ui

import android.os.Bundle
import com.farwolf.currency.R
import com.farwolf.currency.ui.base.BaseActivity
import com.farwolf.currency.ui.currency.CurrencyFragment

class MainActivity : BaseActivity() {

    companion object {
        val TAG: String = MainActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(CurrencyFragment.instance, CurrencyFragment.TAG)
    }

}
