package com.farwolf.currency.ui.currency.item

import com.farwolf.currency.data.model.Currency

data class MoneyExchangerItem(val currency: Currency, var value: Double = 0.0)