package com.farwolf.currency.ui.currency

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.farwolf.currency.R
import com.farwolf.currency.app.CurrencyApp
import com.farwolf.currency.databinding.FragmentCurrencyBinding
import com.farwolf.currency.util.scrollToTop
import org.koin.androidx.viewmodel.ext.android.viewModel

class CurrencyFragment : Fragment() {

    private val currencyViewModel: CurrencyViewModel by viewModel()

    private var currencyAdapter: CurrencyAdapter? = null


    companion object {
        val TAG: String = CurrencyFragment::class.java.simpleName

        val instance: CurrencyFragment
            get() = CurrencyFragment()
    }


    private val adapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount)
            if (toPosition == 0) {
                binding.recyclerView.scrollToTop()
            }
        }
    }

    private lateinit var binding: FragmentCurrencyBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currencyAdapter = CurrencyAdapter
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_currency,
            container,
            false
        )
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()

        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            currencyViewModel = currencyViewModel
        }
        attachObservers()
    }


    private fun initRecyclerView() {
        currencyAdapter?.registerAdapterDataObserver(adapterDataObserver)

        binding.recyclerView.apply {
            adapter = currencyAdapter
            itemAnimator = null
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun attachObservers() {
        currencyViewModel.currencyData.observe(viewLifecycleOwner, Observer {
            CurrencyAdapter.updateCurrency(
                it.rates,
                it.base
            )
        })

        currencyViewModel.errorData.observe(viewLifecycleOwner, Observer {
            Log.e(TAG, it.toString())
        })
    }

    private fun detachObservers() {
        currencyAdapter?.unregisterAdapterDataObserver(adapterDataObserver)
    }

    override fun onResume() {
        super.onResume()
        currencyViewModel.updateCurrency(CurrencyApp.baseCountry)
    }

    override fun onDestroyView() {
        detachObservers()

        // Memory leak
        binding.recyclerView.adapter = null
        super.onDestroyView()
    }

}