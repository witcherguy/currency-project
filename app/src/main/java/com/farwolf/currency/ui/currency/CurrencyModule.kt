package com.farwolf.currency.ui.currency

import com.farwolf.currency.data.repository.CurrencyRepository
import com.farwolf.currency.domain.GetCurrencyUseCase
import com.farwolf.currency.domain.ICurrencyRepository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val currencyModule = module {

    single { CurrencyRepository(get()) as ICurrencyRepository }
    factory { GetCurrencyUseCase(get()) }

    viewModel { CurrencyViewModel(get()) }
}
