package com.farwolf.currency.ui.currency

import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseMethod
import coil.api.load
import coil.transform.CircleCropTransformation
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.farwolf.currency.BR
import com.farwolf.currency.R
import com.farwolf.currency.app.CurrencyApp
import com.farwolf.currency.data.model.Currency
import com.farwolf.currency.ui.base.ViewModelAdapter
import com.farwolf.currency.ui.currency.item.MoneyExchangerItem
import com.farwolf.currency.util.Converter
import com.farwolf.currency.util.focusWithOpenKeyboard
import com.farwolf.currency.util.px


object CurrencyAdapter : ViewModelAdapter() {

    private var baseValue: Double = 1.0
    private var currencyMap: HashMap<Currency, Double>? = null

    init {
        cell(MoneyExchangerItem::class.java, R.layout.item_currency, BR.viewModelItem)
        sharedObject(this, BR.adapter)
    }

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun setImageUrl(view: ImageView, url: String? = null) {
        Glide.with(view)
            .load(url)
            .centerInside()
            .into(view)
    }

    @InverseMethod("currentValueToBaseValue")
    @JvmStatic
    fun baseValueToCurrentValue(view: EditText, oldValue: MoneyExchangerItem, value: MoneyExchangerItem): String {
        return String.format("%.2f", Converter.convertCurrency(baseValue, value))
    }

    @JvmStatic
    fun currentValueToBaseValue(view: EditText, oldValue: MoneyExchangerItem, value: String): MoneyExchangerItem {
        if (view.isFocused) {
            baseValue = Converter.getBaseValue(
                if (value.isEmpty()) {
                    0.0
                } else {
                    value.toDouble()
                }, oldValue
            )
            updateItemExcludeChanged(oldValue)
        }
        return oldValue
    }

    fun itemSelected(view: View, moneyExchangerItem: MoneyExchangerItem) {
        if (CurrencyApp.baseCountry != moneyExchangerItem.currency) {
            CurrencyApp.baseCountry = moneyExchangerItem.currency
            val lastBaseValue = baseValue
            baseValue = lastBaseValue.times(moneyExchangerItem.value)

            val param = lastBaseValue.div(baseValue)

            val currencies = mutableListOf(
                MoneyExchangerItem(
                    CurrencyApp.baseCountry,
                    1.0
                )
            )
            items.forEach {
                it as MoneyExchangerItem
                if (it.currency != CurrencyApp.baseCountry) {
                    currencies.add(
                        MoneyExchangerItem(
                            it.currency,
                            it.value * param
                        )
                    )
                }
            }
            replaceItems(currencies)
        }
        focusToCurrencyInput(view)
    }

    fun updateCurrency(currencyMap: Map<Currency, Double>, baseCurrency: Currency) {
        CurrencyApp.baseCountry = baseCurrency
        this.currencyMap = currencyMap as HashMap
        val currencies = mutableListOf(
            MoneyExchangerItem(
                baseCurrency,
                1.0
            )
        )
        currencies.addAll(currencyMap.map {
            MoneyExchangerItem(
                it.key,
                it.value
            )
        })
        replaceItems(currencies)
    }


    override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
        return (oldItem as MoneyExchangerItem).currency == (newItem as MoneyExchangerItem).currency
    }

    override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
        return (oldItem as MoneyExchangerItem).value == (newItem as MoneyExchangerItem).value
    }

    private fun focusToCurrencyInput(view: View) {
        view.findViewById<EditText>(R.id.currencyValue).focusWithOpenKeyboard()
    }

    private fun updateItemExcludeChanged(moneyExchangerItem: MoneyExchangerItem) {
        val i = items.indexOf(moneyExchangerItem)
        notifyItemRangeChanged(0, i)
        if (i < itemCount - 1) {
            notifyItemRangeChanged(i + 1, itemCount - i - 1)
        }
    }
}