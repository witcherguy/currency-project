package com.farwolf.currency.ui.currency

import android.os.Handler
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.farwolf.currency.app.CurrencyApp
import com.farwolf.currency.data.model.Currency
import com.farwolf.currency.data.model.Exchanger
import com.farwolf.currency.domain.GetCurrencyUseCase
import com.farwolf.currency.util.SingleLiveEvent
import kotlinx.coroutines.launch
import retrofit2.HttpException

class CurrencyViewModel(private val currencyUseCase: GetCurrencyUseCase) : ViewModel() {

    companion object {
        val TAG: String = CurrencyViewModel::class.java.simpleName

        private const val DELAY = 1 * 1000L
    }

    var currencyData = MutableLiveData<Exchanger>()
    var errorData = SingleLiveEvent<Throwable>()

    private var exchanger: Exchanger? = null

    private val handler = Handler()

    private val runnable = Runnable {
        updateCurrency(CurrencyApp.baseCountry)
    }

    private fun startPing() {
        handler.removeCallbacks(runnable)
        handler.postDelayed(runnable, DELAY)
    }

    fun updateCurrency(currency: Currency) {
        viewModelScope.launch {
            try {
                exchanger = currencyUseCase.updateCurrency(currency)
                currencyData.value = exchanger
            } catch (e: HttpException) {
                Log.e(TAG, "Exception ${e.message}")
            } catch (e: Throwable) {
                Log.e(TAG, "Something else went wrong")
            } finally {
                startPing()
            }
        }
    }

    override fun onCleared() {
        handler.removeCallbacks(runnable)
        super.onCleared()
    }
}