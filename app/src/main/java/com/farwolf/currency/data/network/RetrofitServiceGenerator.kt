package com.farwolf.currency.data.network

import com.google.gson.Gson
import com.farwolf.currency.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitServiceGenerator {

    private val httpClient = OkHttpClient.Builder()

    private val builder = Retrofit.Builder()
        .baseUrl(BuildConfig.SERVER_URL)
        .addConverterFactory(GsonConverterFactory.create(Gson()))

    fun <S> createService(serviceClass: Class<S>): S {
        return retrofit().create(serviceClass)
    }

    private fun retrofit(): Retrofit {
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .header("Accept", "application/json")
                .method(original.method(), original.body())
            val request = requestBuilder.build()
            val response = chain.proceed(request)
            response
        }

        val client = httpClient.build()
        return builder.client(client).build()
    }
}