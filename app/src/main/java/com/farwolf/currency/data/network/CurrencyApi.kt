package com.farwolf.currency.data.network

import com.farwolf.currency.data.model.Currency
import com.farwolf.currency.data.model.Exchanger
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET("latest")
    suspend fun getCurrency(@Query("base") currency: Currency): Exchanger
}