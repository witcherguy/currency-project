package com.farwolf.currency.data.network

import org.koin.dsl.module

val networkModule = module {
    single { provideCurrencyService() }
}

fun provideCurrencyService() = RetrofitServiceGenerator.createService(CurrencyApi::class.java)
