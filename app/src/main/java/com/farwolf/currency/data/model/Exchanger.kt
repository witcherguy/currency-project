package com.farwolf.currency.data.model

data class Exchanger(val base: Currency, val rates: Map<Currency, Double>)