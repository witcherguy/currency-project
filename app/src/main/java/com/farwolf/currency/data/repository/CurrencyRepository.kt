package com.farwolf.currency.data.repository

import com.farwolf.currency.data.model.Currency
import com.farwolf.currency.data.model.Exchanger
import com.farwolf.currency.data.network.CurrencyApi
import com.farwolf.currency.domain.ICurrencyRepository

class CurrencyRepository(
    private val currencyApi: CurrencyApi
): ICurrencyRepository {

    override suspend fun getCurrency(currency: Currency): Exchanger {
        return currencyApi.getCurrency(currency)
    }
}