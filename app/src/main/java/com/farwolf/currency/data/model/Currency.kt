package com.farwolf.currency.data.model

enum class Currency(val currency: String, val countryName: String, val flagUrl: String) {
    EUR("EUR", "EUR", "https://www.countryflags.io/eu/flat/64.png"),
    AUD("AUD", "AUD", "https://www.countryflags.io/au/flat/64.png"),
    BGN("BGN", "BGN", "https://www.countryflags.io/bg/flat/64.png"),
    BRL("BRL", "BRL", "https://www.countryflags.io/BR/flat/64.png"),
    CAD("CAD", "CAD", "https://www.countryflags.io/CA/flat/64.png"),
    CHF("CHF", "CHF", "https://www.countryflags.io/CH/flat/64.png"),
    CNY("CNY", "CNY", "https://www.countryflags.io/CN/flat/64.png"),
    CZK("CZK", "CZK", "https://www.countryflags.io/CZ/flat/64.png"),
    DKK("DKK", "DKK", "https://www.countryflags.io/DK/flat/64.png"),
    GBP("GBP", "GBP", "https://www.countryflags.io/GB/flat/64.png"),
    HKD("HKD", "HKD", "https://www.countryflags.io/HK/flat/64.png"),
    HRK("HRK", "HRK", "https://www.countryflags.io/HR/flat/64.png"),
    HUF("HUF", "HUF", "https://www.countryflags.io/HU/flat/64.png"),
    IDR("IDR", "IDR", "https://www.countryflags.io/ID/flat/64.png"),
    ILS("ILS", "ILS", "https://www.countryflags.io/IL/flat/64.png"),
    INR("INR", "INR", "https://www.countryflags.io/IN/flat/64.png"),
    ISK("ISK", "ISK", "https://www.countryflags.io/IS/flat/64.png"),
    JPY("JPY", "JPY", "https://www.countryflags.io/JP/flat/64.png"),
    KRW("KRW", "KRW", "https://www.countryflags.io/KR/flat/64.png"),
    MXN("MXN", "MXN", "https://www.countryflags.io/MX/flat/64.png"),
    MYR("MYR", "MYR", "https://www.countryflags.io/MY/flat/64.png"),
    NOK("NOK", "NOK", "https://www.countryflags.io/NO/flat/64.png"),
    NZD("NZD", "NZD", "https://www.countryflags.io/NZ/flat/64.png"),
    PHP("PHP", "PHP", "https://www.countryflags.io/PH/flat/64.png"),
    PLN("PLN", "PLN", "https://www.countryflags.io/PL/flat/64.png"),
    RON("RON", "RON", "https://www.countryflags.io/RO/flat/64.png"),
    RUB("RUB", "RUB", "https://www.countryflags.io/RU/flat/64.png"),
    SEK("SEK", "SEK", "https://www.countryflags.io/SE/flat/64.png"),
    SGD("SGD", "SGD", "https://www.countryflags.io/SG/flat/64.png"),
    THB("THB", "THB", "https://www.countryflags.io/TH/flat/64.png"),
    TRY("TRY", "TRY", "https://www.countryflags.io/TR/flat/64.png"),
    USD("USD", "USD", "https://www.countryflags.io/US/flat/64.png"),
    ZAR("ZAR", "ZAR", "https://www.countryflags.io/ZA/flat/64.png")


}