package com.farwolf.currency

import com.farwolf.currency.data.model.Currency
import com.farwolf.currency.ui.currency.item.MoneyExchangerItem
import com.farwolf.currency.util.Converter
import org.junit.Assert
import org.junit.Test

class ConverterUnitTest {

    @Test
    fun conversion_isCorrect() {
        Assert.assertEquals(
            13.5,
            Converter.convertCurrency(4.5,
                MoneyExchangerItem(
                    Currency.AUD,
                    3.0
                )
            ),
            0.001
        )
    }
}