package com.farwolf.currency

import androidx.test.espresso.IdlingRegistry
import androidx.test.rule.ActivityTestRule
import com.farwolf.currency.ui.MainActivity
import com.farwolf.currency.util.EspressoIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ExchangerUITest {

    @get:Rule
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun init() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun shouldChangeCurrencyText() {
        EspressoIdlingResource.increment()
    }
}